$(' document ').ready(function() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
});
    
let hardSkills = ["Html", "Css", "Javascript", "Jquery", "Php", "Laravel", "MySql", "Node.js", "MongoDB", "Python"];
hardSkills.forEach(function(hardSkill){
    hsSec.innerHTML +=  `
        <li><a>`+ hardSkill +`</a></li>
    `;
});

let softSkills = ["Listening", "Willingness to learn", "Optimism", "Creative", "Courteous", "Good grasp on English language", "Trainable", "Self-motivated", "Computer Literate", "Dedicated", "Knowledge on Korean languange"];
softSkills.forEach(function(softSkill){
    ssSec.innerHTML +=  `
        <li><a>`+ softSkill +`</a></li>
    `;
});

let interests = ["Movies", "Music", "TV Series", "KDrama", "Kpop", "Mobile Legends", "Dota 2", "Front-end", "Twice", "Marvel Future Fight", "Dancing", "Learning new things"];
interests.forEach(function(interest){
    iSec.innerHTML +=  `
        <li><a>`+ interest +`</a></li>
    `;
});